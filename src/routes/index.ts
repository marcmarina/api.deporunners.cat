import UserRoutes from './user';
import RoleRoutes from './role';
import MemberRoutes from './member';
import TownRoutes from './town';
import TShirtSizeRoutes from './tshirtSize';
import EventRoutes from './event';
import StripeWebhooks from './stripeWebhooks';

export {
  UserRoutes,
  RoleRoutes,
  MemberRoutes,
  TownRoutes,
  TShirtSizeRoutes,
  EventRoutes,
  StripeWebhooks,
};
